﻿<%@ Page Title="Sales Report" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Rapha.Enterprise.MasterSales._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
        </div>
    </div>
    &nbsp;
    <div class="row">
        <asp:GridView ID="SalesGridView" runat="server" DataSourceID="RaphaDataWarehouse"></asp:GridView>
        <asp:SqlDataSource ID="RaphaDataWarehouse" runat="server" ConnectionString="<%$ ConnectionStrings:RaphaDataWarehouseConnectionString %>" SelectCommand="SELECT [Item No_] AS Item, Code, Item_Description FROM Dim_ItemVariant WHERE (Description &lt;&gt; '') ORDER BY Item_Description"></asp:SqlDataSource>
    </div>

</asp:Content>
